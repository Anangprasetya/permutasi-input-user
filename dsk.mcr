cout macro x
	mov ah, 09h
	lea dx, x
	int 21h
endm


cin macro x, p
local terlalu
local lu
	mov ah, 0
	int 16h
	
	mov ah, 02h
	mov dl, al
	int 21h
	
	cmp al, 38h
	ja terlalu
	sub al, 30h
	mov x, al
	xor ax, ax
	jmp lu
	
	
	terlalu:
	cout p
	jmp akhir_program
	
	lu:
endm


kurang macro x, y, r
local terlalu
	xor cx, cx
	mov ch, x
	mov cl, y
	cmp cl, ch
	ja terlalu
	sub ch, cl
	mov y, ch
	jmp ul
	
	
	terlalu:
	cout r
	jmp akhir_program
	
	ul:
endm


cout_per macro x, y
	local tampil
	local stop

	xor bx, bx
	mov bl, x
	tampil:
		mov dl, bl
		add dl, 30h
		mov ah, 02h
		int 21h
		cmp bl, 1
		je stop
		cout y
		dec bl
		cmp bl, 0
		jne tampil
	stop:
endm


hitung macro x, y, r, s
	xor cx, cx
	mov cl, x
	kal:
		xor bx, bx
		mov ax, r 
		mov bl, cl
		mul bx
		mov r, ax
	loop kal
	mov cl, y
	xor bx, bx
	kal_2:
		mov ax, s
		mov bl, cl
		mul bx
		mov s, ax
	loop kal_2
endm

cout_grsPer macro x
	xor cx, cx
	mov cl, x
	mov ah, 02h
	ta:
		mov dl, '-'
		int 21h
		mov dl, '-'
		int 21h
		mov dl, '-'
		int 21h
		mov dl, '-'
		int 21h
	loop ta
endm


hitungBagi macro x, y, r, s
	mov ax, x
	mov r, ax
	mov bx, y
	mov s, bx
	div bx
	mov x, ax
endm


coutAngka macro x
	local an
	local tam
	xor cx, cx
	xor bx, bx
	mov bx, 10
	mov ax, x
	an:
		xor dx, dx
		div bx
		push dx
		inc cx
		cmp ax, 0
		jne an
	
	xor dx, dx
	tam:
		pop dx
		add dl, 30h
		mov ah, 02h
		int 21h
	loop tam
endm

