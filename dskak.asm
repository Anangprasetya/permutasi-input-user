include dsk.mcr

;.model small
;.stack 200h
;.data

.model small
.code
org 100h
mulai:
	jmp prr
	kal_n db 'Nilai n : $'
	kal_r db 10,13,'Nilai r : $'
	er db 10,13,'Angka yang anda inputkan terlalu besar ! $'
	rr db 10,13,'Nilai r tidak boleh lebih dari n !!! $'
	rumu db '===========================',10,13
		db  '|||  P E R M U T A S I  |||',10,13
		db	'|||        n!           |||',10,13
		db  '|||   -----------       |||',10,13
		db  '|||     (n - r)!        |||',10,13
		db  '===========================',10,13
		db  10,13,'$'
	new db 10,13,'$'
	kaa db ' X $'
	samaDengan db ' = $'
	tab db '  $'
	bag db ' / $'
	var_n db 0
	var_r db 0
	t1 dw 1
	t2 dw 1
	t3 dw ?
	t4 dw ?
	
;.code
;mulai:
;	mov ax, @data
;	mov ds, ax

	prr:
	cout new
	cout rumu
	
	cout kal_n
	cin var_n er
	
	cout kal_r
	cin var_r er
	
	cout new
	
	kurang var_n var_r rr
	
	hitung var_n, var_r, t1, t2
	
	hitungBagi t1, t2, t3, t4
	
	cout new
	cout_per var_n kaa
	
	
	cout new
	cout_grsPer var_n
	cout samaDengan
	cout tab
	coutAngka t3
	cout bag
	coutAngka t4
	cout tab
	cout samaDengan
	cout tab
	coutAngka t1
	
	
	cout new
	cout_per var_r kaa
	
	akhir_program:
	cout new
	;mov ax, 4c00h
	;int 21h
	
	int 20h
end mulai
